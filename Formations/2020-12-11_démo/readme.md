# Formation AbidjanTransport - démo et liens

Cette page liste quelques ressources qui ont été réalisées à partir des données du projet et qui ont pu être montrées sous forme de démo lors de la session de formation de décembre 2020.

![Merci à nos partenaires pour leur participation](logo_partenaires.png)

## Plan schématique
![](http://sites.digitaltransport.io/abidjantransport/AbidjanTransport_assets/plan.png)

[Télécharger](https://git.digitaltransport4africa.org/data/africa/abidjan/-/raw/master/Plan/plan_schematique.pdf?inline=false)

Merci à notre partenaire [Latitude Cartagène](https://latitude-cartagene.com/) !

## Application mobile Gbameh
![](capture_gbameh.png)

* [Vidéo de présentation](demo_gbameh.webm)
* [![](https://i.imgur.com/ID3UJ5V.png)](https://play.google.com/store/apps/details?id=com.mrm.gbamehapp&raii=com.mrm.gbamehapp)

Merci à notre partenaire Gbameh !

## Application mobile Trufi pour Abidjan et ChatBot Telegram
![](capture_trufi.png)

* [Télécharger](trufi_abidjan.apk)
* [Vidéo de démonstration](demo_abidjan_trufi.mkv)
* [Vidéo de démonstration](demo_trufiTelegramBot.mp4)

Merci à notre partenaire [Trufi](https://www.trufi-association.org/)

## Carte interactive et calcul d'itinéraire
https://abidjan.preprod.latitude-cartagene.com
![](capture_latitude.png)

Merci à notre partenaire [Latitude Cartagène](https://latitude-cartagene.com/)

## Application mobile OSMAnd
![](capture_osmand.png)

* [![](https://i.imgur.com/ID3UJ5V.png)](https://play.google.com/store/apps/details?id=net.osmand.plus)
* [Vidéo de démonstration](demo_osmand_carto_interactive.webm)
* [Vidéo de démonstration](demo_osmand_iti.webm)

En savoir plus sur [OSMAnd](http://osmand.net/)
![](https://i.imgur.com/3EMUSZB.png)

## Système d'Information Voyageurs navitia

![isochrones](capture_navitia.png)

Merci à notre partenaire [Kisio Digital](https://www.navitia.io/)
