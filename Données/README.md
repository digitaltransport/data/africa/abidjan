# Données

Vous pouvez retrouver ici les données de transport produites dans le cadre du projet initial (2019-2020). Suite à la campagne de mise à jour effectuée en 2021, seul le GTFS a été mis à jour.

Les données sont utilisables sous la licence ODbL aux mêmes conditions que le projet OpenStreetMap. Vous êtes libre de copier, distribuer, transmettre et adapter nos données, à condition que vous créditiez OpenStreetMap et ses contributeurs. Si vous modifiez ou utilisez nos données dans d’autres œuvres dérivées, vous ne pouvez distribuer celles-ci que sous la même licence. [Plus d'informations](https://www.openstreetmap.org/copyright/).

## Données GTFS

Un fichier GTFS décrivant l'offre de transport est disponible. Vous pouvez utiliser l'url suivante pour télécharger la dernière version disponible : https://git.digitaltransport4africa.org/data/africa/abidjan/raw/master/Données/abidjan.zip?inline=false

Ce GTFS est exporté à partir des informations renseignées dans OpenStreetMap.

Ce GTFS a été mis à jour en 2021, suite à de nouveaux relevés sur les réseaux de transport informel (gbaka et woro woro). Le GTFS initial est [disponible ici](https://git.digitaltransport4africa.org/data/africa/abidjan/-/blob/fin_projet/Donn%C3%A9es/abidjan.zip) (attention, les identifiants des objets ont changé).

## Exports

Des export sont disponibles :
- [les lignes](abidjantransport_lignes.csv) ainsi que leurs métadonnées
- [les tracés des lignes](abidjantransport_lignes.geojson)

## Requêtes Overpass

Vous pouvez utiliser [ces requêtes](overpass.md) pour extraire vous-même les dernières informations sur les transport depuis OpenStreetMap.org

## Traces GPS

Vous pouvez retrouver ici [les trajets collectés](abidjantransport_traces.csv) par notre équipe.

Les traces GPS sont stockées sur OpenStreetMap à l'adresse suivante : https://www.openstreetmap.org/traces/tag/abidjantransport

Le [tableur de suivi](tableur de suivi des lignes.html) est le document de travail utilisé par toute l'équipe pour la supervision de la collecte et de la création de données dans OpenStreetMap. Il permet de faire le lien entre les objets dans OSM et les traces GPS collectées.
