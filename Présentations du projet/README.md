# Présentations réalisées

## Septembre 2019

Une [présentation](2019-09-24_Reunion de lancement.pdf) a été réalisée lors de la réunion de lancement du projet, en présence du Ministère des transports, des opérateurs et des syndicats.

## Novembre 2019

Une [présentation](2019-11-23_sotm_africa_2019.pdf) du projet a été réalisée par Jungle Bus et OSM CI lors du [State of The Map Africa](https://2019.stateofthemap.africa/) à Grand Bassam.
